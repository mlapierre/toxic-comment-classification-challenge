TfidfVectorizer(ngram_range=(1,2), tokenizer=tokenize,
             min_df=3, max_df=0.9, strip_accents='unicode', use_idf=1,
             smooth_idf=1, sublinear_tf=1 )
char_vectorizer = TfidfVectorizer(
         sublinear_tf=True,
         strip_accents='unicode',
         analyzer='char',
         ngram_range=(1, 4),
         max_features=20000)            
LogisticRegression(C=4.0, solver='sag')
# Total CV loss is -0.049026422288231314

TfidfVectorizer(ngram_range=(1,2), tokenizer=tokenize,
             min_df=3, max_df=0.9, strip_accents='unicode', use_idf=1,
             smooth_idf=1, sublinear_tf=1 )
char_vectorizer = TfidfVectorizer(
         sublinear_tf=True,
         strip_accents='unicode',
         analyzer='char',
         ngram_range=(1, 4),
         max_features=20000)    
NbSvmClassifier(C=4, dual=False, n_jobs=-1, solver='sag')
# Convergence errors
# Total CV loss is -0.05134072120127755         